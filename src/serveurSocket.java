
import java.io.*;
import java.net.*;

public class serveurSocket extends Thread{
    private ServerSocket serverSocket;

    public serveurSocket(int port) throws IOException {
        serverSocket = new ServerSocket(port);
    }
    public void run() {
        while(true) {
            try {
                System.out.println("En attente d'un client sur le port " + serverSocket.getLocalPort() + "...");
                Socket server = serverSocket.accept();

                System.out.println("Connecté à " + server.getRemoteSocketAddress());

                PrintWriter out = new PrintWriter(server.getOutputStream(), true); // Mise en place sortie
                BufferedReader in = new BufferedReader(new InputStreamReader(server.getInputStream())); // Mise en place entrée

                String line = in.readLine();//récupère la 1re ligne
                System.out.println("Requête : " + line); //Affiche la requête client
                String[] wordsFirstLine = line.split(" "); //récupère la page demandée

                switch (wordsFirstLine[1]){ //Selon la page demandée
                    case "/index.html" :
                        out.println("HTTP/1.0 200 OK"); //Si page accessible
                        break;
                    case "/" :
                        out.println("HTTP/1.0 200 OK"); //Si page accessible
                        break;
                    default :
                        out.println("HTTP/1.0 404 NOT FOUND"); //Si page inaccessible
                        break;
                }

                /*if(wordsFirstLine[1].equals("/index.html") || wordsFirstLine[1].equals("/")){
                    out.println("HTTP/1.0 200 OK"); //Si page accessible
                } else {
                    out.println("HTTP/1.0 404 NOT FOUND"); //Si page inaccessible
                }*/
                out.println("Content-Type: text/html; charset=UTF-8");
                out.println("Date: Thu, 09 Sep 2021 05:48:06 GMT");
                out.println("Server: jsp");
                out.println("Content-Length: 218");
                out.println("");
                //out.println("<HTML><HEAD><meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\">");
                //out.println("<TITLE>Nom du site</TITLE></HEAD><BODY>");

                switch (wordsFirstLine[1]){ //Selon la page demandée
                    case "/index.html" :
                    case "/" :
                        try(BufferedReader br = new BufferedReader(new FileReader("index.html"))) {
                            String ligneFichier;
                            while ((ligneFichier = br.readLine()) != null) {
                                out.println(ligneFichier);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                    default :
                        try(BufferedReader br = new BufferedReader(new FileReader("erreur404.html"))) {
                            String ligneFichier;
                            while ((ligneFichier = br.readLine()) != null) {
                                out.println(ligneFichier);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                }

                /*if(wordsFirstLine[1].equals("/index.html")){ //si le document demandé est : index.html
                    out.println("<h1>Voici la page index.html ?</h1>");
                } else if(wordsFirstLine[1].equals("/")){ //si le document demandé est : /
                    out.println("<h1>Voici la page d'accueil du site.</h1>");
                } else {
                    out.println("<h1>Erreur 404 : Cette page est inaccessible. Désolé.</h1>");
                }*/
                out.println("</BODY></HTML>");
                server.close();

            } catch (IOException e) {
                e.printStackTrace();
                break;
            }
        }
    }

    public static void main(String[] args) {
        int port = Integer.parseInt(args[0]);
        try {
            Thread t = new serveurSocket(port);
            t.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
