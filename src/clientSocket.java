
import java.io.*;
import java.net.*;

public class clientSocket {

    public static Socket connection(Socket clientSocket, String serverName){
        try {
            clientSocket = new Socket(serverName, 80);
            System.out.println("Connecté à " + clientSocket.getRemoteSocketAddress());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return clientSocket;
    }

    public static void browseLine(Socket clientSocket, String[] args) {
        try {
            System.out.println("Le serveur a renvoyé : ");

            BufferedReader in = null; // Mise en place entrée
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            String line = null;

            line = in.readLine();
            String[] wordsFirstLine = line.split(" ");
            switch (wordsFirstLine[1]){
                case "200" :
                    if(args[2].equalsIgnoreCase("affiche")){
                        browseLineAffichage(line, in); //Parcours les lignes de la réponse
                    }else if(args[2].equalsIgnoreCase("sauvegarde")){
                        browseLineSauvegarde(line, in, args); //Parcours les lignes de la réponse
                    }
                    break;
                case "401" :
                    System.out.println("Erreur 401 : utilisateur non authentifié.");
                    break;
                case "404" :
                    System.out.println("Erreur 404 : page non trouvée.");
                    break;
                default :
                    System.out.println("Erreur inconnue.");
                    break;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void browseLineAffichage(String line, BufferedReader in){
        try {
            while( line != null ) {
                System.out.println(line);
                line = in.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void browseLineSauvegarde(String line, BufferedReader in, String[] args) {
        try {
            String fileNameModif = args[1].replace("/", "-");
            File file = new File(args[0] + fileNameModif + ".txt");
            FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
            BufferedWriter bw = new BufferedWriter(fw);

            while( line != null ) {
                bw.write(line + "\n");
                line = in.readLine();
            }
            bw.close();
            System.out.println("Sauvegarde terminée.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void affichagePage(Socket clientSocket, String[] args){
        try {
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true); // Mise en place sortie

            //envoie de la commande
            out.println("GET " + args[1] + " HTTP/1.0");
            out.println();
            System.out.println("La requête client a été envoyée au serveur.");

            browseLine(clientSocket, args); //Parcours les lignes de la réponse
            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Socket clientSocket = new Socket();

        clientSocket = connection(clientSocket, args[0]); //args[0]

        affichagePage(clientSocket, args); //args[1]
    }
}


